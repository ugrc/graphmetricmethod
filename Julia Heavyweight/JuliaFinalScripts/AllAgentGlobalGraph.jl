using CSV
using DataFrames
using StatsBase

unfilteredData = DataFrame(CSV.File("../../../Data/Dataset/processedKaggle.csv"))
dateTimes = unfilteredData[!,:datetime]

function extractDate(x)
    if x === missing
        return "nodate"
    end
    m = match(r"(?<date>\d{4}-\d{2}-\d{2}) .*",x)
    if m == nothing
        return "nodate"    
    else
        return m[:date]
    end
end

dates = map(x->extractDate(x), dateTimes)
insertcols!(unfilteredData,:datetime,:date=>dates)


function filterOutNonURLs(x)
    if x === missing
        return false
    end
    if occursin(r"(/static/|/image/|/settings/logo|robots|[.](png|jpg|css|js|ico))",x) || occursin(r"HEAD",x)
        return false
    else
        return true
    end
end

fullData = subset(unfilteredData, :request => ByRow(x->passmissing(filterOutNonURLs)(x)), skipmissing=true)

groupByIPDate = groupby(fullData,[:ip,:date])
weightedGraph = Dict()

for g in groupByIPDate
    firstRequest = true
    prevReq = ""
    for rec in eachrow(g)
        if firstRequest
            firstRequest = false
            continue
        end
        edge = rec[:request] => prevReq
        count = get!(weightedGraph,edge, 0)  
        weightedGraph[edge] = count+1
        prevReq = rec[:request]
    end
end


source = map(x ->first(first(x)), sortedGraph)
dest = map(x ->last(first(x)), sortedGraph)
weight = map(x ->last(x), sortedGraph)

graphDf = DataFrame(source=source, destination=dest, weight=weight)

function threshNormalize(X)
    Z = Float64.(X)
    mark = quantile(X,0.999)
    println(mark)
    Z = map(x->min(mark, x)/mark, Z)
    return Z
end

insertcols!(graphDf,:weight,:normWeights=>threshNormalize(graphDf[!,:weight])) 

CSV.write("../../../Data/Graph/allWeightedGraph.csv", graphDf) 