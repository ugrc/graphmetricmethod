using CSV
using DataFrames
using StatsBase
using Plots


unfilteredData = DataFrame(CSV.File("../../../Data/Dataset/processedKaggle.csv"))
dateTimes = unfilteredData[!,:datetime]

function extractDate(x)
    if x === missing
        return "nodate"
    end
    m = match(r"(?<date>\d{4}-\d{2}-\d{2}) .*",x)
    if m == nothing
        return "nodate"    
    else
        return m[:date]
    end
end

dates = map(x->extractDate(x), dateTimes)
insertcols!(unfilteredData,:datetime,:date=>dates)


function filterOutNonURLs(x)
    if x === missing
        return false
    end
    if occursin(r"(/static/|/image/|/settings/logo|robots|[.](png|jpg|css|js|ico))",x) || occursin(r"HEAD",x)
        return false
    else
        return true
    end
end

fullData = subset(unfilteredData, :request => ByRow(x->passmissing(filterOutNonURLs)(x)), skipmissing=true)

bots = subset(fullData, :useragent => ByRow(x -> occursin(r"bot|AMPHTML|Cloudflare-AMP", x)||x=="-"), skipmissing=true) 
realUsers = subset(fullData, :useragent => ByRow(x -> !(occursin(r"bot|AMPHTML|Cloudflare-AMP", x)||x=="-")), skipmissing=true) 


serverGraph = Dict()
serverDf = DataFrame(CSV.File("../../Data/Graph/allWeightedGraph.csv"))
for row in eachrow(serverDf)
    serverGraph[row.source=>row.destination] = row.normWeights
end


function getIpGraphs(groupedDf)
    ipGraphs = Dict()

    for g in groupedDf
        weightedGraph = Dict()

        firstRequest = true
        prevReq = ""
        ip = ""
        date = ""
        for rec in eachrow(g)
            if firstRequest
                firstRequest = false
                ip = rec[:ip]
                date = rec[:date]
                continue
            end
            edge = rec[:request] => prevReq
            count = get!(weightedGraph,edge, 0)  
            weightedGraph[edge] = count+1
            prevReq = rec[:request]
        end

        sortedGraph = sort(collect(weightedGraph), by=x->x[2], rev=true)

        source = map(x ->first(first(x)), sortedGraph)
        dest = map(x ->last(first(x)), sortedGraph)
        weight = map(x ->last(x), sortedGraph)

        graphDf = DataFrame(source=source, destination=dest, weight=weight)
        ipGraphs[ip=>date] = graphDf
    end
    return ipGraphs
end



function ipGraphSimilarity(gDf)
    positiveScore = 0
    penaltyScore = 0
    penWeighting = 0
    posWeighting = 0

    gamma = 0.05

    edgeWeightDict = Dict()
    for row in eachrow(gDf)
        edgeWeightDict[row.source=>row.destination] = 1
    end

    for (edge, ipWeight) in pairs(edgeWeightDict)
        serverWeight = get(serverGraph, edge, 0)
        positiveScore += (serverWeight)^2.5
        posWeighting += (serverWeight)^1.5
        if serverWeight != 1
            penaltyScore += (1-serverWeight)*sqrt(serverWeight)
            penWeighting += sqrt(serverWeight)
        end
    end
    return gamma*(positiveScore/posWeighting) + (1-gamma)*(1-penaltyScore/penWeighting)
end

function positiveScore(gDf)
    positiveScore = 0
    penaltyScore = 0
    penWeighting = 0
    posWeighting = 0

    gamma = 0.05

    edgeWeightDict = Dict()
    for row in eachrow(gDf)
        edgeWeightDict[row.source=>row.destination] = 1
    end

    for (edge, ipWeight) in pairs(edgeWeightDict)
        serverWeight = get(serverGraph, edge, 0)
        positiveScore += (serverWeight)^2.5
        posWeighting += (serverWeight)^1.5
        if serverWeight != 1
            penaltyScore += (1-serverWeight)*sqrt(serverWeight)
            penWeighting += sqrt(serverWeight)
        end
    end
    return (positiveScore/posWeighting)
end

function penaltyScore(gDf)
    positiveScore = 0
    penaltyScore = 0
    penWeighting = 0
    posWeighting = 0

    gamma = 0.05

    edgeWeightDict = Dict()
    for row in eachrow(gDf)
        edgeWeightDict[row.source=>row.destination] = 1
    end

    for (edge, ipWeight) in pairs(edgeWeightDict)
        serverWeight = get(serverGraph, edge, 0)
        positiveScore += (serverWeight)^2.5
        posWeighting += (serverWeight)^1.5
        if serverWeight != 1
            penaltyScore += (1-serverWeight)*sqrt(serverWeight)
            penWeighting += sqrt(serverWeight)
        end
    end
    return (1-penaltyScore/penWeighting)
end


function getUrlsAndUnique(ipaddr, filterGroup)
    selectGroup = filter([:ip,:date]=>(x,y)->(x[1]=>y[1])==ipaddr,filterGroup)
    urls = nrow(selectGroup[1])
    uniqueUrls = nrow(unique(selectGroup[1],:request))
    return urls=>uniqueUrls
end


groupByIP = groupby(realUsers,[:ip, :date])
ipCount = combine(groupByIP, nrow => :count)
highUsers = filter(row-> row.count>300, ipCount)
highUserSet = Set()
for row in eachrow(highUsers)
    push!(highUserSet, row.ip=>row.date)
end
groupByIPfiltered = filter([:ip,:date]=>(x,y)->(x[1]=>y[1]) in highUserSet, groupByIP)


ipGraphsuser = getIpGraphs(groupByIPfiltered)


similarityResultsUser = Dict()
posResUser = Dict()
penResUser = Dict()
urlsUser = Dict()
numUniqueUser = Dict()
for (ipaddr, df) in pairs(ipGraphsuser)
    similarityResultsUser[ipaddr] =  ipGraphSimilarity(df)
    posResUser[ipaddr] =  positiveScore(df)
    penResUser[ipaddr] =  penaltyScore(df)
    urlsUniques = getUrlsAndUnique(ipaddr, groupByIPfiltered)
    urlsUser[ipaddr] = first(urlsUniques)
    numUniqueUser[ipaddr] = last(urlsUniques)
end

simUserDf = DataFrame(:ip=>collect(keys(similarityResultsUser)),
    :score=>collect(values(similarityResultsUser)), 
    :pos=>collect(values(posResUser)), 
    :pen=>collect(values(penResUser)), 
    :numUrl=>collect(values(urlsUser)),
    :numUniqueUser=>collect(values(numUniqueUser))
)
sort!(simUserDf, :score)
CSV.write("../../../Data/Graph/userLinkSim.csv", simUserDf)

gr()
non_bot_fig = histogram(sort(collect(values(similarityResultsUser))),bins = 40, normalize=false)
png("non_bot_fig")

non_bot_fig_lower = histogram(sort(collect(values(similarityResultsUser)))[1:50],bins = 20, normalize=false)
png("non_bot_fig_lower")


groupByIPbot = groupby(bots,[:ip,:date])
ipCountbot = combine(groupByIPbot, nrow => :count)
highUsersbot = filter(row-> row.count>300, ipCountbot)
highUserSetbot = Set()
for row in eachrow(highUsersbot)
    push!(highUserSetbot, row.ip=>row.date)
end
groupByIPfilteredbot = filter([:ip,:date]=>(x,y)->(x[1]=>y[1]) in highUserSetbot, groupByIPbot)

ipGraphsbot = getIpGraphs(groupByIPfilteredbot)

similarityResultsBot = Dict()
posResBot = Dict()
penResBot = Dict()
urlsBot = Dict()
numUniqueBot = Dict()

for (ipaddr, df) in pairs(ipGraphsbot)
    similarityResultsBot[ipaddr] =  ipGraphSimilarity(df)
    posResBot[ipaddr] =  positiveScore(df)
    penResBot[ipaddr] =  penaltyScore(df)  
    urlsUniques = getUrlsAndUnique(ipaddr, groupByIPfilteredbot)
    urlsBot[ipaddr] = first(urlsUniques)
    numUniqueBot[ipaddr] = last(urlsUniques)
end

botUserDf = DataFrame(:ip=>collect(keys(similarityResultsBot)),
:score=>collect(values(similarityResultsBot)),
:pos=>collect(values(posResBot)), 
:pen=>collect(values(penResBot)),
:numUrl=>collect(values(urlsBot)),
:numUniqueUser=>collect(values(numUniqueBot))
)
sort!(botUserDf, :score)
CSV.write("../../Data/Graph/botLinkSim.csv", botUserDf)

bots_hist = histogram(sort(collect(values(similarityResultsBot))),bins = 20, normalize=false)
png("bots_hist")

bots_hist_lower = histogram(sort(collect(values(similarityResultsBot)))[1:100],bins = 10, normalize=false)
png("bots_hist_lower")