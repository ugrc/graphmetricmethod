import pandas as pd
import networkx as nx
from pyvis.network import Network

bot_data = pd.read_csv("../../Data/Graph/botWeightedGraph.csv",header=0)

bot_data = bot_data[bot_data["weight"] > 2]

ids = 0

mapping = {}

for n in bot_data["source"]:
    if n not in mapping:
        mapping[n] = ids
        ids+=1

for n in bot_data["destination"]:
    if n not in mapping:
        mapping[n] = ids
        ids+=1

# bot_data["source"] = bot_data["source"].map(mapping)
# bot_data["destination"] = bot_data["destination"].map(mapping)

print(bot_data)

G = nx.from_pandas_edgelist(bot_data, source="source", target="destination", edge_attr="weight")

nx.write_gexf(G, "../../Data/Graph/botVis.gexf")