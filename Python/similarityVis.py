import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

userSim = pd.read_csv("../../Data/Graph/userLinkSim.csv",header=0)
botSim = pd.read_csv("../../Data/Graph/botLinkSim.csv",header=0)

plt.hist([userSim["pos"][:200],botSim["pos"]], color=['r','b'], alpha=0.5, density=True)
# sns.histplot(userSim["score"], color='blue', alpha=0.5)
# sns.histplot(botSim["score"], color='red', alpha=0.5)
plt.show()

