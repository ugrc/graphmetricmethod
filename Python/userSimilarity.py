import pandas as pd
import networkx as nx
from pyvis.network import Network

def process_graph(g):
    g = g[g["weight"] > 5]
    g = g.dropna()
    return nx.from_pandas_edgelist(g, source="source", target="destination", edge_attr="normWeights")

server_graph = pd.read_csv("../../Data/Graphs/userWeightedGraph.csv",header=0)
server_graph_nx = process_graph(server_graph)

user_graph = pd.read_csv("../../Data/Graphs/GraphByIP/GraphOF91.99.72.15.csv",header=0)
user_graph_nx = process_graph(user_graph)

