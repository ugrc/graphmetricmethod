import pandas as pd
import networkx as nx
from pyvis.network import Network

user_data = pd.read_csv("../../Data/Graph/userWeightedGraph.csv",header=0)

user_data = user_data[user_data["weight"] > 5]
user_data = user_data[user_data["source"] != None]

ids = 0

mapping = {}

for n in user_data["source"]:
    if n not in mapping:
        mapping[n] = ids
        ids+=1

for n in user_data["destination"]:
    if n not in mapping:
        mapping[n] = ids
        ids+=1

user_data = user_data.dropna()

# bot_data["source"] = bot_data["source"].map(mapping)
# bot_data["destination"] = bot_data["destination"].map(mapping)

print(user_data)

G = nx.from_pandas_edgelist(user_data, source="source", target="destination", edge_attr="weight")

nx.write_gexf(G, "../../Data/Graph/userVis.gexf")