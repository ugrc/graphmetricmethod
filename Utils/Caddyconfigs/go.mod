module mymodule

go 1.18

require (
	github.com/dsparling/go-apache-log-parser v0.0.0-20170910184549-5610fa21018e // indirect
	github.com/lmorg/apachelogs v0.0.0-20161115121556-e5f3eae677ad // indirect
	github.com/satyrius/gonx v1.3.0 // indirect
)
