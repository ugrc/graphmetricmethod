package main

import  (
	"fmt"
	"os"
    "bufio"
    "regexp"
    "time"
    "encoding/csv"
)

var num_correct int

// type row struct{
//     Ip string `json:"ip"`
//     Request string `json:"request"`
//     Useragent string `json:"useragent"`
//     DateTime time.Time `json:"datetime`
//     StatusCode int `json:"statuscode"`
//     BytesSent int `json:"bytessent"`
// }


func main() {
    f, err := os.Open("access.log")
    if err != nil {
        fmt.Println("cannot able to read the file", err)
        return
    }
    // UPDATE: close after checking error
    defer f.Close()  //Do not forget to close the file

    of, erro := os.Create("processed.csv")
    if erro != nil {
        fmt.Println("cannot able to create the file", erro)
        return
    }
    // UPDATE: close after checking error
    defer of.Close()  //Do not forget to close the file
    csvWriter := csv.NewWriter(of)
    csvWriter.Write([]string{"ip","request","useragent","datetime", "statuscode", "bytessent"})

    r := regexp.MustCompile("(?P<ipaddress>\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}) - (?P<user>(\\-)|(.+)) \\[(?P<dateandtime>\\d{2}\\/[A-Za-z]{3}\\/\\d{4}:\\d{2}:\\d{2}:\\d{2} (\\+|\\-)\\d{4})\\] ([\"](?P<request>.*)[\"]) (?P<statuscode>\\d{3}) (?P<bytessent>\\d+) ([\"](?P<refferer>(\\-)|(.*))[\"]) ([\"](?P<useragent>.*)[\"]) ([\"](?P<unknown>.*)[\"])")

    lineReader := bufio.NewReader(f)

    processNum := 0
    const timeFormat = "02/Jan/2006:15:04:05 -0700"

    for {

        if processNum%100000 ==0 {
            fmt.Printf("%d\n", processNum)
        }
        processNum++


        line, err := lineReader.ReadString('\n')
        if err != nil { // io.EOF
            break
        }
//     fmt.Printf("%#v\n", r.FindStringSubmatch(line))
        match := r.FindStringSubmatch(line)
        if len(match) == 0{
            fmt.Println(line)
            continue
        }
        result := make(map[string]string)
        for i, name := range r.SubexpNames() {
        if i != 0 && name != "" {
                result[name] = match[i]
            }
        }
        parsedTime, errTime := time.Parse(timeFormat, result["dateandtime"])

        if errTime!=nil{
            fmt.Println(errTime)
        }

        record := []string{result["ipaddress"], result["request"], result["useragent"], parsedTime.String(), result["statuscode"], result["bytessent"]}

        csvWriter.Write(record)
    }

    csvWriter.Flush()
    fmt.Println("done")

}
