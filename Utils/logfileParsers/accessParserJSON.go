package main

import  (
	"fmt"
	"os"
    "bufio"
    "regexp"
    "encoding/json"
    "time"
    "strconv"
)

var num_correct int

type row struct{
    Ip string `json:"ip"`
    Request string `json:"request"`
    Useragent string `json:"useragent"`
    DateTime time.Time `json:"datetime`
    StatusCode int `json:"statuscode"`
    BytesSent int `json:"bytessent"`
}


func main() {
    f, err := os.Open("access.log")
    if err != nil {
        fmt.Println("cannot able to read the file", err)
        return
    }
    // UPDATE: close after checking error
    defer f.Close()  //Do not forget to close the file

    of, erro := os.Create("processed.json")
    if erro != nil {
        fmt.Println("cannot able to create the file", erro)
        return
    }
    // UPDATE: close after checking error
    defer of.Close()  //Do not forget to close the file
    bufioWriter := bufio.NewWriter(of)

    separator := []byte(",\n")
    bufioWriter.Write([]byte("[\n"))

    r := regexp.MustCompile("(?P<ipaddress>\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}) - (?P<user>(\\-)|(.+)) \\[(?P<dateandtime>\\d{2}\\/[A-Za-z]{3}\\/\\d{4}:\\d{2}:\\d{2}:\\d{2} (\\+|\\-)\\d{4})\\] ([\"](?P<request>.*)[\"]) (?P<statuscode>\\d{3}) (?P<bytessent>\\d+) ([\"](?P<refferer>(\\-)|(.*))[\"]) ([\"](?P<useragent>.*)[\"]) ([\"](?P<unknown>.*)[\"])")

    lineReader := bufio.NewReader(f)

    processNum := 0
    const timeFormat = "02/Jan/2006:15:04:05 -0700"

    first_element := true

    for {

        if processNum%100000 ==0 {
            fmt.Printf("%d\n", processNum)
        }
        processNum++


        line, err := lineReader.ReadString('\n')
        if err != nil { // io.EOF
            break
        }
//     fmt.Printf("%#v\n", r.FindStringSubmatch(line))
        match := r.FindStringSubmatch(line)
        if len(match) == 0{
            fmt.Println(line)
            continue
        }
        result := make(map[string]string)
        for i, name := range r.SubexpNames() {
        if i != 0 && name != "" {
                result[name] = match[i]
            }
        }
        codeInt, _ := strconv.Atoi(result["statuscode"])
        bytesInt, _ := strconv.Atoi(result["bytessent"])
        parsedTime, errTime := time.Parse(timeFormat, result["dateandtime"])

        if errTime!=nil{
            fmt.Println(errTime)
        }

        serialized, _ := json.MarshalIndent(row{Ip:result["ipaddress"], Request:result["request"], Useragent:result["useragent"], DateTime:parsedTime, StatusCode:codeInt, BytesSent:bytesInt}, "", "    ")

        if !first_element{
                    bufioWriter.Write(separator)
        }else{
            first_element = false
        }
        bufioWriter.Write(serialized)
    }

    bufioWriter.Write([]byte("\n]"))
    bufioWriter.Flush()
    fmt.Println("done")

}
