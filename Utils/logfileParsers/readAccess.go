package main

import  (
	"fmt"
	"os"
	"bufio"
)

var num_correct int

// func callbackPrintFn(line *apachelogs.AccessLine){
// // 	fmt.Printf("remote host: %s\n", line.IP)
// // 	fmt.Printf("time: %s\n", line.DateTime.String())
// // 	fmt.Printf("request: %s\n", line.Method)
// // 	fmt.Printf("status: %d\n", line.Status)
// // 	fmt.Printf("bytes: %d\n", line.Size)
// // 	fmt.Printf("referer: %s\n", line.Referrer)
// // 	fmt.Printf("user agent: %s\n", line.UserAgent)
// // 	fmt.Printf("url: %s\n", line.URI)
// 	num_correct++
// 	if num_correct%100000 == 0 {
// 		fmt.Printf("Processed %d\n",num_correct)
// 	}
// }
//
// func errorCallback(err error){
// 	num_error++
// }

func main() {
    f, err := os.Open("access.log")
    if err != nil {
        fmt.Println("cannot able to read the file", err)
        return
    }
    // UPDATE: close after checking error
    defer f.Close()  //Do not forget to close the file

    lineReader := bufio.NewReader(f)
    for i:=0; i<45; i++{
        line, err := lineReader.ReadString('\n')
        if err != nil { // io.EOF
            break
        }
        fmt.Println(line)
    }
// num_error :=0
// apachelogs.ReadAccessLog("access.log", callbackPrintFn, errorCallback)
//
// fmt.Printf("\nNum error %d", num_error)
}

// "%{IPORHOST:clientip} - %{USERNAME:remote_user} \[%{HTTPDATE:time_local}\] \"%{QS:request}\" %{INT:status} %{INT:body_bytes_sent} \"%{QS:http_user_agent}\" \"%{QS:http_referer}\""}
